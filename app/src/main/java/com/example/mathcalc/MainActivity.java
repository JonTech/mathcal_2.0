package com.example.mathcalc;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button matrixBtn;
    Button socatoaBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void pushBtn(View view) {
        matrixBtn = findViewById(R.id.mtxbtn);
        socatoaBtn = findViewById(R.id.socabtn);

        if (view.getId() == R.id.mtxbtn) {
            Intent i = new Intent(this, MatrixCalc.class);
            startActivity(i);
        }
        else if (view.getId() == R.id.socabtn) {
            Intent i = new Intent(this, SocatoaCalc.class);
            startActivity(i);
        }
    }
}
